package com.incentro.sa.models;

public class ValueLabel {
    private int value;
    private String label;
    
    //Replaced Constructor with Factory Method
    private ValueLabel(int value, String label) {
        this.value = value;
        this.label = label;
    }
    
    public static ValueLabel createValueLabel(int value, String label) {
        return new ValueLabel(value, label);
    }
    
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
    public void addToValue(int value) {
        this.value = this.value + value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "ValueLabel{" +
                "value=" + value +
                ", label='" + label + '\'' +
                '}';
    }
}

package com.incentro.sa.models;

import java.util.Date;

public class EmailObjectBuilder {
    private String user;
    private Date theDate;
    private String messID;
    private String from;
    private String sub;
    
    public EmailObjectBuilder setUser(String user) {
        this.user = user;
        return this;
    }
    
    public EmailObjectBuilder setTheDate(Date theDate) {
        this.theDate = theDate;
        return this;
    }
    
    public EmailObjectBuilder setMessID(String messID) {
        this.messID = messID;
        return this;
    }
    
    public EmailObjectBuilder setFrom(String from) {
        this.from = from;
        return this;
    }
    
    public EmailObjectBuilder setSub(String sub) {
        this.sub = sub;
        return this;
    }
    
    public EmailObject createEmailObject() {
        return new EmailObject(user, theDate, messID, from, sub);
    }
}